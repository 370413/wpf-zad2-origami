(*
Autor: Tomasz Necio
Code Review: Jakub Jasiulewicz
*)


(* * * * TYPY * * * *)


(* point implementuje punkty i wektory na płaszczyźnie xy *)
type point = float * float

(* linie proste reprezentować będziemy jako pary point * point *)

type kartka = point -> int

type strona_prostej = Lewa | DokladnieNa | Prawa


(* * * * WARTOŚCI DO WYJĄTKÓW * * * *)


(* wartość zwracana, gdy łapany jest wyjątek przy próbie utworzenia
   niepoprawnej kartki *)
let bledna_kartka p = -1


(* * * * DZIAŁANIA NA WEKTORACH (PUNKTACH) * * * *)


let poczatek_ukladu = (0., 0.)

(* obracanie wektora o 90 stopni przeciw wskazówkom zegara *)
let prostopadly (x1, y1) = ((-.y1), x1)

(* mnożenie wektora przez skalar *)
let razy skalar (x1, y1) = (skalar *. x1, skalar *. y1)

(* wartość iloczynu wektorowego *)
let war_il_wekt (x1, y1) (x2, y2) = (x1 *. y2) -. (x2 *. y1)

(* liczymy kwadrat odległości, żeby móc później unikać błędów zaokrągleń
   zmiennoprzecinkowych wynikających z pierwiastkowania tam gdzie to możliwe *)
let kwadrat_odleglosci (x1, y1) (x2, y2) =
    ((x1 -. x2) ** 2.) +. ((y1 -. y2) ** 2.)

(* długość wektora jest jego odległością od zera *)
let kwadrat_normy = kwadrat_odleglosci poczatek_ukladu


(* * * * FUNKCJE DO KONSTRUKCJI KARTEK * * * *)


(* funkcja zadaje kartkę prostokątną *)
let prostokat (x1, y1) (x2, y2) =
    try 
        (* sprawdzanie poprawności danych *)
        if not ((x1 <= x2) && (y1 <= y2)) then
            failwith "Podano niepoprawne punkty"
        else        
            fun (xp, yp) ->
                if x1 <= xp && xp <= x2 && y1 <= yp && yp <= y2 then 1 else 0
    with
    |  Failure(s) -> print_string (s ^ "\n"); bledna_kartka
    
(* funkcja zadaje kartkę okrągłą *)
let kolko p r =
    try
        (* sprawdzenie poprawności;
           zapis z not wyłapuje od razu nie tylko ujemne r, ale też NaN *)
        if not (r >= 0.) then
            failwith "Podano niepoprawny promień"
        else
            fun punkt ->
                if kwadrat_odleglosci punkt p <= (r ** 2.) then 1 else 0
    with
    |  Failure(s) -> print_string (s ^ "\n"); bledna_kartka


(* * * * FUNKCJE DO DZIAŁAŃ NA PUNKTACH I PROSTYCH * * * *)


(* wektor wskazujący kierunek i 'zwrot' prostej _nieunormowany_ *)
let wektor_kierunkowy ((x1, y1), (x2, y2)) = (x2 -. x1, y2 -. y1)

(* wskazuje z której strony prostej jest dany punkt
   używając własności iloczynu wektorowego *)
let strona punkt prosta =
    let wektor1 = wektor_kierunkowy ((fst prosta), punkt) in
    let a = war_il_wekt wektor1 (wektor_kierunkowy prosta) in
    if a > 0. then Prawa else if a < 0. then Lewa else DokladnieNa

(* funkcja znajdująca położenie punktu po przerzuceniu go przez prostą *)
let odbicie punkt prosta = 
    let nieunorm_odleglosc = 
        (*         
         Obliczamy odległość punktu od prostej wzorem z laboratorium;
         p1, p2 - punkty określające prostą; s - szukana odległość;
         wówczas pole trójkąta (p1, p2, punkt) = (d(p1, p2) * s) / 2;
         co po prostych przekształceniach daje:
         war_il_wekt wektor(p1, punkt) (unormowany wektor_kier. prosta) = s.
        *)
        let wektor1 = wektor_kierunkowy ((fst prosta), punkt) in
        (* 
           Nie normujemy tej odległości przez wektor kierunkowy prostej, bo
           zauważamy, że przesunięcie trzeba później unormować przez wektor
           kierunkowy prostej obróconej o 90 st., którego norma jest taka sama
           jak norma wektora kierunkowego oryg. prostej - można więc użyć
           obliczanego dokładniej kwadratu normy 
        *)
        war_il_wekt wektor1 (wektor_kierunkowy prosta)
    in
    let przesuniecie =
        (* Przesuwamy punkt w kierunku prostopadłym do prostej o dwukrotność
           jego odległości od niej *)
        razy
            ( 2. *. nieunorm_odleglosc /.
             (kwadrat_normy (wektor_kierunkowy prosta))
            )
            (prostopadly (wektor_kierunkowy prosta))
    in
        ((fst punkt) +. (fst przesuniecie), (snd punkt) +. (snd przesuniecie))


(* * * * FUNKCJE SKŁADAJĄCE KARTKI * * * *)


let zloz p1 p2 k =
    try
        (* sprawdzanie poprawności danych *)
        if (p1 = p2) then
            failwith "Podane punkty nie definiują jednoznacznie prostej"
        else if (k p1 < 0) then
            failwith "Podano błedną kartkę"
        else
            let prosta = (p1, p2) in
                fun punkt ->
                    match strona punkt prosta with
                    (* przebicie kartki po złożeniu przebije punkt po lewej
                       stronie złożenia tyle razy ile przedtem + tyle ile
                       wcześniej przebijało punkt który znajdzie się w tym
                       samym miejscu po złożeniu *)
                    | Lewa -> (k punkt) + (k (odbicie punkt prosta))
                    (* interpretacja przebicia punktu dokładnie na prostej
                       zgodnie z instrukcją *)
                    | DokladnieNa -> k punkt
                    (* punkty po prawej znajdą się poza kartką *)
                    | Prawa -> 0
    with
    |  Failure(s) -> print_string (s ^ "\n"); bledna_kartka

let skladaj l k =
    try
        let f acc (p1, p2) =
            if (acc p1 < 0) then
                (* to sprawdzenie zapobiega zapętlonemu podawania błędnej
                   kartki w kolejnych iteracjach *)
                failwith "Nie można dalej prowadzić operacji na błędnej kartce"
            else
                zloz p1 p2 acc
        in
        (* fold_left od razu składa kartkę w dobrej kolejności stosując złóż
           na kolejnych punktach na liście *)
            List.fold_left f k l
    with
    | Failure(s) -> print_string (s ^ "\n"); bledna_kartka
